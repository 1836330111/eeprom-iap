#include "common.h"
uint8_t CheckXor(uint8_t *Buf,uint8_t Len)
{
  uint8_t i =0;
  uint8_t Xor =0;
  
  for(i=0; i<Len; i++)
  {
    Xor = Xor^(*(Buf+i));
  }
  
  return Xor;
}

uint8_t CheckSum(uint8_t *Buf,uint8_t Len)
{
  uint8_t i =0;
  uint8_t sum =0;
  uint8_t checksum =0;
  
  for(i=0; i<Len; i++)
  {
    sum += *Buf++;
  }
  checksum = sum &0xff;
  return checksum;
}

void BubbleSort(uint8_t a[],int n)//n为数组a的元素个数
{
    //最多进行N-1轮比较
    for(int i=0; i<n-1; i++)
    {
        bool isSorted = true;
        //每一轮比较前n-1-i个，即已排序好的最后i个不用比较
        for(int j=0; j<n-1-i; j++)
        {
            if(a[j] > a[j+1])
            {
                isSorted = false;
                int temp = a[j];
                a[j] = a[j+1];
                a[j+1]=temp;
            }
        }
        if(isSorted) break; //如果没有发生交换，说明数组已经排序好了
    }
}


