#ifndef _COMMON_H
#define _COMMON_H
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
uint8_t CheckXor(uint8_t *Buf,uint8_t Len);
uint8_t CheckSum(uint8_t *Buf,uint8_t Len);
void BubbleSort(uint8_t a[],int n);
#endif

