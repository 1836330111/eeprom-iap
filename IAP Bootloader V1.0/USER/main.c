#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "led.h"
#include "key.h"
#include "lcd.h"
#include "usmart.h"
#include "stmflash.h"
#include "spi.h"
#include "iap.h" 
#include "w25qxx.h"
#include "24cxx.h"
#include "common.h"
/************************************************
 ALIENTEK 探索者STM32F407开发板 实验50
 串口IAP实验Bootlaoder-HAL库函数版
 技术支持：www.openedv.com
 淘宝店铺：http://eboard.taobao.com 
 关注微信公众平台微信号："正点原子"，免费获取STM32资料。
 广州市星翼电子科技有限公司  
 作者：正点原子 @ALIENTEK
************************************************/
//EEPROM 24C02只有256字节容量
u8 Rbuff[6] ={0x01,0x01,0x03,0x15,0x02,0x06};
int main(void)
{
	u8 t;
	u8 key;
	u16 oldcount=0;				    //老的串口接收数据值
	u32 applenth=0;				    //接收到的app代码长度
	u8 clearflag=0; 
	u8 lens;	
	
	u16 addr = 0;
	u8 datatemp[5000];
    u16 id = 0;	
	
	
	
	
	
    HAL_Init();                   	//初始化HAL库    
    Stm32_Clock_Init(336,8,2,7);  	//设置时钟,168Mhz
	delay_init(168);               	//初始化延时函数
	uart_init(115200);             	//初始化USART
	LED_Init();						//初始化LED	
	KEY_Init();						//初始化KEY
 	LCD_Init();           			//初始化LCD
	
    W25QXX_Init();				    //W25QXX初始化	
	AT24CXX_Init();				    //初始化IIC 

 	POINT_COLOR=RED;//设置字体为红色 
	LCD_ShowString(30,50,200,16,16,"Explorer STM32F4");	
	LCD_ShowString(30,70,200,16,16,"IAP TEST");	
	LCD_ShowString(30,90,200,16,16,"ATOM@ALIENTEK");
	LCD_ShowString(30,110,200,16,16,"2017/5/8");  
	LCD_ShowString(30,130,200,16,16,"KEY_UP:Copy APP2FLASH");
	LCD_ShowString(30,150,200,16,16,"KEY1:Erase SRAM APP");
	LCD_ShowString(30,170,200,16,16,"KEY0:Run SRAM APP");
	LCD_ShowString(30,190,200,16,16,"KEY2:Run FLASH APP");
	POINT_COLOR=BLUE;//设置字体为蓝色	




			printf("CheckSum: %02x \n",CheckSum(Rbuff,6));
			printf("CheckSum: %02x \n",CheckXor(Rbuff,6));

	BubbleSort(Rbuff,6);
	for(lens = 0;lens<6;lens++)
		printf("BubbleSort:%02x \r\n",Rbuff[lens]);






//	while(1)
//	{
//		id = W25QXX_ReadID();
//        printf("W25QXX_ReadID %d\r\n",id);
//		if (id == W25Q128 || id == NM25Q128)
//			break;
//		LCD_ShowString(30,150,200,16,16,"W25Q128 Check Failed!");
//		delay_ms(500);
//		LCD_ShowString(30,150,200,16,16,"Please Check!        ");
//		delay_ms(500);
//		LED0=!LED0;		//DS0闪烁
//	}
while(1)
	{
	 	if(USART_RX_CNT)
		{
			if(oldcount==USART_RX_CNT)//新周期内,没有收到任何数据,认为本次数据接收完成.
			{
				applenth=USART_RX_CNT;
				oldcount=0;
				USART_RX_CNT=0;
				printf("用户程序接收完成!\r\n");
				printf("代码长度:%dBytes\r\n",applenth);
				for(lens = 0;lens<5;lens++)
					printf("earlyUSART_RX_BUF:%02x \r\n",USART_RX_BUF[lens]);	
				for(lens = 5;lens>0;lens--)
					printf("lastUSART_RX_BUF:%02x \r\n",USART_RX_BUF[applenth-lens]);
			}else oldcount=USART_RX_CNT;			
		}
		t++;
		delay_ms(10);
		if(t==30)
		{
			LED0=!LED0;
			t=0;
			if(clearflag)
			{
				clearflag--;
				if(clearflag==0)LCD_Fill(30,210,240,210+16,WHITE);//清除显示
			}
		}	  	 
		key=KEY_Scan(0);
		if(key==WKUP_PRES)	//WK_UP按键按下
		{
				printf("开始更新固件...\r\n");	

//					W25QXX_Read(datatemp,0,6);
//					for(lens = 0;lens<4;lens++)
//					printf("datatemp:%d \r\n",datatemp[lens]);					
//					W25QXX_Write("65431",0,6);
//					W25QXX_Read(datatemp,0,6);
					
#if 0		//写1 读0			
					AT24CXX_Write(addr,(u8*)USART_RX_BUF,applenth);	
					AT24CXX_Read(addr,datatemp,applenth);	
				for(lens = 0;lens<10;lens++)
					printf("earlydatatemp:%02x \r\n",datatemp[lens]);	
				for(lens = 10;lens>0;lens--)
					printf("lastdatatemp:%02x \r\n",datatemp[applenth-lens]);	
#endif
					
#if 0  //写0 读1
					AT24CXX_Read(0,datatemp,1024);		
					iap_write_appbin(FLASH_APP1_ADDR+0,datatemp,1024);//更新FLASH代码 
					
					AT24CXX_Read(1024,datatemp,1024);					
					iap_write_appbin(FLASH_APP1_ADDR+1024,datatemp,1024);//更新FLASH代码 
					
					AT24CXX_Read(2048,datatemp,1024);
					iap_write_appbin(FLASH_APP1_ADDR+2048,datatemp,1024);//更新FLASH代码 
					
					AT24CXX_Read(3072,datatemp,1024);
					iap_write_appbin(FLASH_APP1_ADDR+3072,datatemp,1024);//更新FLASH代码 
					
					AT24CXX_Read(4096,datatemp,368);
					iap_write_appbin(FLASH_APP1_ADDR+4096,datatemp,368);//更新FLASH代码	
					
//					LCD_ShowString(30,210,200,16,16,"Copy APP Successed!!");
					printf("固件更新完成!\r\n");	
#endif					
					printf("addr:%d + applens:%d\r\n",addr,applenth);						
					addr = addr+applenth;
				
					applenth = 0;								 
		}

		if(key==KEY2_PRES)	//KEY2按下
		{
			printf("开始执行FLASH用户代码!!\r\n");
			if(((*(vu32*)(FLASH_APP1_ADDR+4))&0xFF000000)==0x08000000)//判断是否为0X08XXXXXX.
			{	 
				iap_load_app(FLASH_APP1_ADDR);//执行FLASH APP代码
			}else 
			{
				printf("非FLASH应用程序,无法执行!\r\n");
				LCD_ShowString(30,210,200,16,16,"Illegal FLASH APP!");	   
			}									 
			clearflag=7;//标志更新了显示,并且设置7*300ms后清除显示	  
		}
			   
		 
	}
}



